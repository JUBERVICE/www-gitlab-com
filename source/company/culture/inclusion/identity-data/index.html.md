---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data as of 2019-09-30

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 66        | 7.24%           |
| Based in EMEA                               | 257       | 28.21%          |
| Based in LATAM                              | 12        | 1.32%           |
| Based in NORAM                              | 576       | 63.23%          |
| **Total Team Members**                      | **911**   | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 665       | 73.00%          |
| Women                                       | 246       | 27.00%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **911**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 39        | 78.00%          |
| Women in Leadership                         | 11        | 22.00%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **50**    | **100%**        |

| **Gender in Development**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Development                          | 332       | 83.00%          |
| Women in Development                        | 68        | 17.00%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **400**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.18%           |
| Asian                                       | 31        | 5.69%           |
| Black or African American                   | 21        | 3.85%           |
| Hispanic or Latino                          | 31        | 5.69%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 22        | 4.04%           |
| White                                       | 316       | 57.98%          |
| Unreported                                  | 124       | 22.75%          |
| **Total Team Members**                      | **546**   | **100%**        |

| **Race/Ethnicity in Development (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 11        | 6.75%           |
| Black or African American                   | 4         | 2.45%           |
| Hispanic or Latino                          | 10        | 6.13%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 4.29%           |
| White                                       | 101       | 61.96%          |
| Unreported                                  | 30        | 18.40%          |
| **Total Team Members**                      | **163**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 6         | 15.00%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 1         | 2.50%           |
| White                                       | 23        | 57.50%          |
| Unreported                                  | 10        | 25.00%          |
| **Total Team Members**                      | **38**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 1         | 0.11%           |
| Asian                                       | 73        | 8.01%           |
| Black or African American                   | 26        | 2.85%           |
| Hispanic or Latino                          | 46        | 5.05%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 29        | 3.18%           |
| White                                       | 500       | 54.88%          |
| Unreported                                  | 236       | 25.91%          |
| **Total Team Members**                      | **911**   | **100%**        |

| **Race/Ethnicity in Development (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 36        | 9.00%           |
| Black or African American                   | 7         | 1.75%           |
| Hispanic or Latino                          | 23        | 5.75%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 12        | 3.00%           |
| White                                       | 224       | 56.00%          |
| Unreported                                  | 98        | 24.50%          |
| **Total Team Members**                      | **400**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 6         | 12.00%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 2.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 1         | 2.00%           |
| White                                       | 27        | 54.00%          |
| Unreported                                  | 15        | 30.00%          |
| **Total Team Members**                      | **50**    | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 16        | 1.76%           |
| 25-29                                       | 175       | 19.21%          |
| 30-34                                       | 264       | 28.98%          |
| 35-39                                       | 173       | 18.99%          |
| 40-49                                       | 190       | 20.86%          |
| 50-59                                       | 84        | 9.22%           |
| 60+                                         | 8         | 0.88%           |
| Unreported                                  | 1         | 0.11%           |
| *Total Team Members*                        | *911*     | *100%*          |


Source: GitLab's HRIS, BambooHR
