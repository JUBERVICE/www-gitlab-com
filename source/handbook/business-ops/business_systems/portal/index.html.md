---
layout: handbook-page-toc
title: "Business Systems: Portal"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Contacts

[Business Systems Administrator \| Phil Encarnacion](/job-families/finance/business-system-analyst/)

[Business Systems Analyst \| Jamie Carey](/job-families/finance/business-system-analyst/)

## Related Documentation and Links: Licensing, Billing, Transactions
* [Troubleshooting subscription and licensing problems](/handbook/support/workflows/license_troubleshooting.html)
* [License documentation](https://docs.gitlab.com/ee/user/admin_area/license.html)
* [Customer Portal: Admin internal documentation](/handbook/internal-docs/customers-admin/)
* [Epic: Analysis: License and Billing Queue](https://gitlab.com/groups/gitlab-com/business-ops/bizops-bsa/-/epics/6)

## Responsibilities, Expectations, & Work on the L&R Queue in Zendesk
The customer portal intersects with Support, Billing, Sales, and Product primarily.
Tickets come into the L&R queue in Zendesk generally three ways:
   - by emailing renewals@gitlab.com
   - by [opening a support ticket](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=334487) and selecting `Help with my license`
   - by filling out the form on the [Renewals page](https://about.gitlab.com/renewals/)
As the License and Renewals (L&R) Queue is a queue of Zendesk tickets outside of the scope of support to the GitLab product, it's handled by the Business Systems Specialist (BSS) in order to take control of a pain point that touches many departments, analyze the tickets, and identify and execute on solutions with the appropriate departments.
The BSS works closely with the Support team since they use the same tool and their work often overlaps.

Currently, it's expected that most incoming tickets are classified as low priority.
When the BSS is in off hours, Sales will often search the L&R queue and take tickets relating to IACV. 

Expectations:
* Meet first SLA
* **Passing to Sales:** Pass tickets relating to IACV to Sales via cc to Sales in reply to ticket which sends an email to both customer and account owner. If an account is associated with the ticket, there's a ticket object in SFDC.
It's then Sales's responsibility to continue the customer interaction and the BSS closes the Zendesk ticket.
* **Passing to Sales:** If a ticket comes in and the BSS is unable to determine the account owner or the inquiry is related to new business, the BSS reaches out to the SFDC chatter group `ZD-newbusiness` where the SDR Regional leads for EMEA, APAC and AMER/LATAM provide an answer.
Then the BSS cc's that person on the ticket and closes in Zendesk.
   * Regional Leads for SDRs: 
      *  AMER/LATAM - Mona Elliot
      *  APAC - Jay Thomas-Burrows
      *  EMEA - Elsje Smart
* **Passing to Billing:** The mechanism to pass tickets to the Billing team is to open the ticket, then change the field `form` from the value `Upgrades, Renewals & AR (refunds)` to `Accounts Receivable/Refunds`.
This puts the ticket into the Billing Team's queue which they then manage.
* Escalations for L&R tickets should follow the [same process](/handbook/support/internal-support/#i-want-to-draw-attention-to-an-existing-support-ticket) as for Support tickets.
* There is a slack channel `#support_fulfillment` for help, but it's strongly encouraged to open a ticket in Zendesk for customers to receive the most prompt help or to refer to existing documentation for general questions.

### Common Customer Inquiries
Tickets are first triaged by the BSS and then sometimes passed to Sales, .com Support, or Billing; [Complete list](https://docs.google.com/spreadsheets/d/1pLyG3pDG5rDuMv5gnAlMBba_nDXwNt6e4VUaQf7dY9c/edit#gid=1318259161) of ticket categories and subcategories (internal documentation)

#### Business Systems Specialist:
*  User wants to trial a plan other than Gold on GitLab.com (BSS first triages as appropriate, then passes to .com Support [Internal Issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request)
*  User receives an error during the purchasing process within the customers portal, initially triaged by BSS, then if needed Fulfillment
*  User wants the red renewal approaching banner message in their Self-Managed system removed, customer needs to upload working license
*  User doesn't know the steps to purchase a GitLab.com subscription
*  User doesn't see their group during purchase process, they need to create their group or associate their GitLab.com account with their portal account. If there's an error, send to .com Support
*  User doesn't understand how true-up works
*  Instructions for activating the license key
*  User wants to know when they will receive the license key
*  User doesn't renew paid Self-Managed plan, what happens to the license and features, BSS triages with initial explanation and also cc's Sales
*  A customer reports problems when registering their license key

#### inquiries routed to Sales:
*  User reports an inability to upgrade from one paid plan to another, cc Account Owner on reply to customer + internal note asking BSS to let BSS know they got it and can close case
*  User is on a trial and wants to purchase a paid plan, cc Account Owner on reply to customer + internal note asking BSS to let BSS know they got it and can close case
*  Customer wants to pay by anything other than a credit card, cc Account Owner on reply to customer + internal note asking BSS to let BSS know they got it and can close case

#### inquiries routed to .com Support:
*  User wants to trial a plan other than Gold on GitLab.com (BSS first triages as appropriate, then passes to .com Support [Internal Issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request)
*  User wants to extend GitLab.com trial [Internal Issue](https://gitlab.com/gitlab-com/support/dotcom/dotcom-internal/issues/new?issuable_template=plan_change_request) 

#### inquiries routed to Accounts Receivable: (Billing Requests)
*  User wants to downgrade subscription, queue in Zendesk
*  Copy of invoice
*  Changes to invoice (address, company name, VAT #, etc)
*  Refund requests
*  Requests to make a payment/payment failed

---

## High Level Setup

[video of custom setup](https://drive.google.com/drive/folders/1kfCEQM6XYGWYxq3Ke4TNvtmDR-46erVD)

### Customer Flow

<img src="/handbook/business-ops/images/Customer_flow1.png" class="full-width">

### System Integrations

Important to know

* [READ ME customer portal repo](https://gitlab.com/gitlab-org/customers-gitlab-com#subscription-portal-app)
* API should send country codes ISO 2 from portal to SFDC and Marketo

<img src="/handbook/business-ops/images/portal_integration3.png" class="full-width">

### Zuora and Salesforce

<img src="/handbook/business-ops/images/zuoraSFDC1.png" class="full-width">
