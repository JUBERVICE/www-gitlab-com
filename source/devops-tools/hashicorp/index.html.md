---
layout: markdown_page
title: "HashiCorp"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Positioning
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary
HashiCorp is a software company with a Freemium business model based in San Francisco, California. HashiCorp provides open-source tools and commercial products that enable developers, operators and security professionals to provision, secure, run and connect cloud-computing infrastructure. HashiCorp's core capabilities include:

- **Infrastructure Automation (Terraform)** - Use infrastructure as code to consistently provision any cloud, infrastructure, and service 
- **Security Automation (Vault)** - Secure, store and tightly control access to tokens, passwords, certificates, encryption keys for protecting secrets and other sensitive data using a UI, CLI, or HTTP API
- **Networking Automation (Consul)** - A multi-cloud service networking platform to connect and secure services across any runtime platform and public or private cloud
- **Application Automation (Nomad)** - An easy-to-use, flexible, and performant workload orchestrator that enables organizations to deploy applications on any infrastructure at scale



GitLab is a complete DevOps platform, delivered as a single application that includes not only configuration management, but also capabilities for project management, source code management, CI/CD, and monitoring. GitLab is designed for Kubernetes and cloud native applications.

## Resources
* [HashiCorp Terraform](https://www.terraform.io/)
* [HashiCorp Vault](https://www.hashicorp.com/products/vault)
* [HashiCorp Consul](https://www.hashicorp.com/products/consul)
* [HashiCorp Nomad](https://www.hashicorp.com/products/nomad/)
* [HashiCorp Packer](https://www.packer.io/)
* [HashiCorp Vagrant](https://www.vagrantup.com/)

